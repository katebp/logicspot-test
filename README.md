# LogicSpot Test

A shopping basket example page built for LogicSpot using HTML5, CSS3 and ES6 JavaScript

http://logicspot.katecodes.co.uk/

## Built With

*  [SASS](https://sass-lang.com/)
*  [Gulp](https://gulpjs.com/)


SASS and Gulp were used to generate the CSS. Source .scss files are available to view in /assets/scss/. 

The CSS and JavaScript has not been minified for ease of checking.


## Further development

Form validation should be added to ensure the user does not manually enter figures outside of the accepted range (1-10).

For improved performance, the following steps could be considered:

*  When the basket is empty a message directing the user back to the site should display
*  Sprites and minified CSS & JavaScript should be used to minimise page load

### Design notes

* The progress bar at the top of the screen uses different icons for 2 and 3 as the SVGs were unavailable
* Used Montserrat rather than Gotham for the input field text, as it was a Google font.
