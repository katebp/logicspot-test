const formatter = new Intl.NumberFormat("en-UK", {
    style: "currency",
    currency: "GBP",
    minimumFractionDigits: 2
});

function updateTable() {
    const subtotalsArray = document.querySelectorAll("table.products tr:not(.removed) td.subtotal");
    var subtotals = 0;
    subtotalsArray.forEach(function(item) {
        subtotals = subtotals + parseInt(item.innerHTML.replace(/[£,]+/g,""));
    });
    
    //update total minus VAT
    document.querySelector("td.total-subtotal").innerHTML = formatter.format(subtotals);

    //calculate VAT at 20%
    var vatvalue = subtotals / 100 * 20;
    document.querySelector("td.total-vat").innerHTML = formatter.format(vatvalue);

    //update final amount
    document.querySelector("td.total-value").innerHTML = formatter.format(subtotals + vatvalue);
}

function submitBasket(event) {
    event.stopPropagation();
    event.preventDefault();

    let productTable = document.querySelectorAll("table.products tbody tr:not(.removed)");

    var i;
    for (i = 0; i < productTable.length; i++) { // output values from table to console
        console.log("item id: " + productTable[i].getAttribute("id"));
        console.log("item count: " + productTable[i].querySelector("input").value);
        console.log("item subtotal: " + productTable[i].querySelector("td.subtotal").textContent);
        // --> add other neccessary details here <--
    }

    // display thank you message 
    document.querySelector("section.my-basket").innerHTML = "<div class='thanks'><p>Your order has been successfully submitted!</p></div>";
}

// increase / decrease value in product value input field
let productvals = document.querySelectorAll(".product-quantity");
productvals.forEach(function(item) {
    var productInput = item.getElementsByTagName("input")[0];
    var productValue = productInput.value;
    var productPrice = Number(productInput.getAttribute("data-item-price"));
    var productInput = productInput.id; // storing for session use

    item.querySelector(".quantity-decrease-button").addEventListener("click", function(){
        if (productValue > 1) { // don't allow user to reduce items to lower than 1
            productValue--;
            item.getElementsByTagName("input")[0].value = productValue; 
            // --> here's where we'd push the new value to the session <--

            // update total price for row
            var formatPrice = Number(productValue) * productPrice;
            item.nextElementSibling.innerHTML = formatter.format(formatPrice);
            updateTable();
        }
    });
    item.querySelector(".quantity-increase-button").addEventListener("click", function(){
        if (productValue < 10) { // don't allow user to reduce items to more than 10
            productValue++;
            item.getElementsByTagName("input")[0].value = productValue; 
            // --> here's where we'd push the new value to the session <--

            //update total price for row
            var formatPrice = Number(productValue) * productPrice;
            item.nextElementSibling.innerHTML = formatter.format(formatPrice);
            updateTable();
        }
    });

    item.querySelector(".quantity-remove").addEventListener("click", function(){
        item.parentNode.classList="removed";
        setTimeout(function(){
            item.parentNode.style.display="none";
          }, 400);
          updateTable();

        // --> here's where we'd push the new data to the session <!--

        const itemsLeftInBasket = document.querySelector("table.products tbody tr:not(.removed)");

        if (itemsLeftInBasket === null) {
            document.querySelector(".cta-checkout").disabled = true;
        }
    });
});

const checkoutcta = document.querySelector(".cta-checkout").addEventListener("click", function(event) {
    submitBasket(event);
})