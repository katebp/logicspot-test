'use strict';

var gulp = require("gulp");
var sass = require("gulp-sass");

//compile
gulp.task('sass', function() {
    return gulp.src('assets/scss/*.scss')
      .pipe(sass())
      .pipe(gulp.dest('assets/style'))
  })

//compile and watch
gulp.task('watch', function(){
    gulp.watch('assets/scss/*.scss', gulp.series('sass')); 
});